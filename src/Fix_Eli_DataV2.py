#!/usr/bin/env_python3

"""Script to fix Eli data and change date with 7 days, 14 hours and 33 minutes"""

__author__ = 'Elibeth Alberts'

import sys
import pandas as pd 
from datetime import timedelta
from datetime import datetime
import os


# open and read multiple files from cmnd promp
df_list = []

for raspberry_data in sys.argv[1:]:
    df = pd.read_csv(raspberry_data, error_bad_lines=False, header=None, index_col=None)
    df_list.append(df) 

    # change date
    dates = df[0]
    new_list = []   
    for line in dates:
        datetime_object = line.split(".")[0]
        datetime_object2 = datetime.strptime(datetime_object, '%Y-%m-%d %H:%M:%S')
        modified_date = str(datetime_object2 + timedelta(days=7, hours=14, minutes=33))
        new_list.append(modified_date)
    df[0] = new_list 
    df = df.set_index(0) # set date as index

    # export to new file location
    df.to_csv('New_data/NewE_' + raspberry_data)

