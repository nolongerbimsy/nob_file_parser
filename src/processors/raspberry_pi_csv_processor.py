import pandas as pd
from processors.abstract_general_list_processor import AbstractGeneralListProcessor


class RaspberryPiCSVListProcessor(AbstractGeneralListProcessor):
    """
    Process all smaller data frames into a single useable data frame of Raspberry Pi data.
    """

    def __init__(self, dict_of_owner_data_frames: dict, start_time=None, end_time=None):
        """
        Process all the smaller Raspberry pi data frames into a single one.
        :param list_of_data_frames: All data frames for a specific data type.
        :param start_time: String representing the starting date which should be used.
        :param end_time: String representing the end date which should be used.
        """
        self.raspberry_pi_data_frames = dict_of_owner_data_frames
        self.void_process_data(dict_of_owner_data_frames, start_time, end_time)

    def void_process_data(self, dict_of_owner_data_frames: dict, start_time, end_time) -> None:
        """
        Runs the whole process of forming a large Raspberry pi data frame.
        :param list_of_data_frames: All data frames for a specific data type.
        :param start_time: String representing the starting date which should be used.
        :param end_time: String representing the end date which should be used.
        """
        # Starts as a dict and by iterating through each key a list can be retrieved.
        for owner in dict_of_owner_data_frames:
            # Concatenate the list of data frames together into a single data frame.
            owners_data_frame = pd.concat(dict_of_owner_data_frames[owner])
            owners_data_frame = self.assign_date_time_index(owners_data_frame, date_time_column=0)
            # Select range of date and time.
            owners_data_frame = owners_data_frame.loc[start_time:end_time]
            owners_data_frame = owners_data_frame[~owners_data_frame.index.duplicated(keep="first")]
            dict_of_owner_data_frames[owner] = self.assign_data_type_and_interpolate(owners_data_frame)

    def get_data_frames(self) -> dict:
        """
        Gives back a single data frame of Raspberry pi data.
        :return: Single data frame of all data available about the Raspberry Pi data.
        """
        return self.raspberry_pi_data_frames
