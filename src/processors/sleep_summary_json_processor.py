import pandas as pd
from processors.abstract_general_list_processor import AbstractGeneralListProcessor


class SleepSummaryJSONProcessor(AbstractGeneralListProcessor):
    """
    Process all smaller data frames into a single useable data frame of sleep summary data.
    """

    def __init__(self, list_of_data_frames: list, start_time=None, end_time=None):
        """
        Process all the smaller sleep summary data frames into a single one.
        :param list_of_data_frames: All data frames for a specific data type.
        :param start_time: String representing the starting date which should be used.
        :param end_time: String representing the end date which should be used.
        """
        self.data_frame = None
        self.void_process_data(list_of_data_frames, start_time, end_time)

    def void_process_data(self, list_of_data_frames: list, start_time, end_time) -> None:
        """
        Runs the whole process of forming a large sleep summary data frame.
        :param list_of_data_frames: All data frames for a specific data type.
        :param start_time: String representing the starting date which should be used.
        :param end_time: String representing the end date which should be used.
        """
        # Concatenate the list of data frames together into a single data frame.
        self.data_frame = pd.concat(list_of_data_frames, sort=False)
        self.data_frame = self.assign_data_type_and_interpolate(self.data_frame, interpolate_data=False)

    def get_data_frame(self) -> pd.DataFrame:
        """
        Gives back a single data frame of sleep summary data.
        :return: Single data frame of all data available about the sleep data.
        """
        return self.data_frame
