from abc import ABC, abstractmethod
import pandas as pd


class AbstractGeneralListProcessor(ABC):
    """
    Abstract class setting a layout for the processors.
    """

    @abstractmethod
    def void_process_data(self, data: object, start_time: object, end_time: object) -> None:
        """
        None, overwritten by subclass.
        """
        pass

    @staticmethod
    def assign_date_time_index(data_frame: pd.DataFrame, date_time_column=None) -> pd.DataFrame:
        """
        Assign index to the correct column in a concatenated data frame.
        :param data_frame: Data frame for a single type of data.
        :param date_time_column: Column index number.
        :return: Data frame with the index set as date time.
        """
        if isinstance(date_time_column, int) and date_time_column < len(data_frame.columns):
            data_frame.set_index(pd.DatetimeIndex(data_frame[data_frame.columns[date_time_column]]),
                                 inplace=True)
            # Drop old date time column.
            data_frame.drop(data_frame.columns[date_time_column], axis=1, inplace=True)
            data_frame.index.name = None

        return data_frame

    @staticmethod
    def assign_data_type_and_interpolate(owners_data_frame: pd.DataFrame, interpolate_data=True) -> pd.DataFrame:
        """
        Interpolate the time per second and sets all data types to float64
        for faster processing and allowing NaN values within the columns.
        :param owners_data_frame: Data frame from a specific owner with a certain type of data. (Heart,sleep etc..)
        :param interpolate_data: Boolean which determines if the data should be interpolated at all.
        :return: Data frame with data that is interpolated or not based on value of interpolate_data.
        """
        # Assign correct data types for performance.
        owners_data_frame = owners_data_frame.astype(
            dict(zip(owners_data_frame.columns,
                     [len(owners_data_frame.columns) * " float64"][0].split(" ")[1:])))
        # Interpolate data.
        if interpolate_data:
            owners_data_frame = owners_data_frame.resample('S').ffill()
            owners_data_frame = owners_data_frame.interpolate(method='linear')
        # Sort the index for just in case.
        return owners_data_frame.sort_index()
