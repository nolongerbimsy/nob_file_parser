#!/usr/bin/env python3

import os.path
import zipfile
from zipfile import ZipFile
import pandas as pd
from parsers.profile_parsers import json_profile_parser, csv_profile_parser
from parsers.json_parsers import hearth_rate_json_parser, sleep_json_parser, sleep_summary_json_parser
from parsers.csv_parsers import sleep_score_csv_parser

__author__ = "Sylt Schuurmans"
__copyright__ = "Copyright 2019, Sleep_inertia_web_app No longer Bimsy"
__license__ = ""
__version__ = "0.1"
__status__ = "Production"


class FitBitZipFileExplorer:
    """
    Opens a zip file and collects all relevant sleep, heart-rate, sleep score and profile data.
    It produces 3 lists (heart-rate, sleep, sleep_summary) with data frames and one single separate data frame (sleep score).
    Each gets a column assigned with an owner based on the profile name used.
    """

    def __init__(self, zip_file: str) -> None:
        """
        Goes through the zip file and looks for the relevant files and passes these files to the correct parsers.
        :param zip_file: containing the FitBit data.
        """
        self._profile_owner = None
        self._list_of_json_heart_rate_data_frames = []
        self._list_of_json_sleep_data_frames = []
        self._list_of_json_sleep_summary_data_frames = []
        self._sleep_csv_score_data_frame = None
        self.void_zip_file_parser(zip_file)

    def void_zip_file_parser(self, zip_file: str) -> None:
        """
        Iterates through the FitBit zip file and calls parsers based on the filenames it encounters within the zip,
        results are stored in lists.
        :param zip_file: FitBit zip file.
        """
        with zipfile.ZipFile(zip_file) as opened_zip_file:
            # Get the profile from the file because it has to be processed first.
            path_to_profile = self.get_location_of_profile_in_zip(opened_zip_file)
            self._profile_owner = self.read_owner_from_file(path_to_profile, opened_zip_file)
            file_names_in_zip = opened_zip_file.namelist()
            file_names_in_zip.remove(path_to_profile)
            # Check all other files within the zip.
            for file_path_name in file_names_in_zip:
                file_name = os.path.split(file_path_name)[-1]
                with opened_zip_file.open(file_path_name) as file_in_zip:
                    if file_name.split("/")[0].startswith("heart_rate-"):
                        self._list_of_json_heart_rate_data_frames.append(
                            hearth_rate_json_parser.HeartRateJSONParser(file_in_zip).get_data_frame())
                    elif "sleep-" in file_name:
                        self._list_of_json_sleep_data_frames.append(
                            sleep_json_parser.SleepJSONParser(file_in_zip).get_data_frame())
                        # Open the file twice because they look at separate things within the heart_rate_json.
                        with opened_zip_file.open(file_path_name) as same_file_in_zip:
                            self._list_of_json_sleep_summary_data_frames.append(
                                sleep_summary_json_parser.SleepSummaryJSONParser(same_file_in_zip).get_data_frame())
                    elif "sleep_score" in file_name:
                        self._sleep_csv_score_data_frame = sleep_score_csv_parser.SleepScoreCSVParser(
                            file_in_zip).get_data_frame()

    @staticmethod
    def get_location_of_profile_in_zip(opened_zip_file: ZipFile) -> str:
        """
        Finds the location of a file called profile file within the zip that is produced by FitBit.
        :param opened_zip_file: Connection to opened zip file.
        :return: The path to a file called profile within the zip.
        """
        return opened_zip_file.namelist()[
            [os.path.split(zip_file_in_zip)[-1].lower().split(".")[0] for zip_file_in_zip in
             opened_zip_file.namelist()].index("profile")]

    @staticmethod
    def read_owner_from_file(profile_file_name: str, opened_zip_file: ZipFile) -> str:
        """
        Activates a profile parser based on the file extension and retrieves the profile owner name from it.
        :param profile_file_name: The filename of the profile, a CSV and JSON version have been observed.
        :param opened_zip_file: Entry point to zipfile.
        :return: Name of the profile owner.
        """
        # Open the file that is within the zip.
        with opened_zip_file.open(profile_file_name) as profile_in_zip:
            file_extension = os.path.split(profile_file_name)[-1].split(".")[-1].lower()
            if file_extension == "csv":
                _profile_owner = csv_profile_parser.CSVProfileParser(profile_in_zip).get_profile_name()
            elif file_extension == "json":
                _profile_owner = json_profile_parser.JSONProfileParser(profile_in_zip).get_profile_name()
        return _profile_owner

    def get_json_heart_rate_data_frames(self) -> list:
        """
        Gives back a list with heart-rate data frames.
        :return: List of data frames with heart-rate info.
        """
        return self._list_of_json_heart_rate_data_frames

    def get_json_sleep_data_frames(self) -> list:
        """
        Gives back a list with sleep data frames.
        :return: List of data frames with sleep info.
        """
        return self._list_of_json_sleep_data_frames

    def get_json_sleep_summary_data_frames(self) -> list:
        """
        Gives back a list with sleep_summary data frames.
        :return: List of data frames with sleep_summaries.
        """
        return self._list_of_json_sleep_summary_data_frames

    def get_csv_sleep_score_data_frame(self) -> pd.DataFrame:
        """
        Gives back a sleep_score data frame.
        :return: Sleep_score data frame.
        """
        return self._sleep_csv_score_data_frame

    def get_profile_owner(self) -> str:
        """
        Gives back the profile name.
        :return: String of profile name.
        """
        return str(self._profile_owner)
