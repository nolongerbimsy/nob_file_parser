from abc import ABC, abstractmethod


class AbstractZipExplorer(ABC):
    """
    Abstract class for zip explorer.
    """

    @abstractmethod
    def void_zip_file_parser(self, zip_file: str) -> None:
        """
        None, overwritten by subclass.
        :param zip_file: Zip file.
        """
        pass
