import zipfile
import pandas as pd
from explorers.abstract_zip_explorer import AbstractZipExplorer
from parsers.csv_parsers.raspberry_pi_csv_parser import RaspberryPiParser

__author__ = "Sylt Schuurmans"
__copyright__ = "Copyright 2019, Sleep_inertia_web_app No longer Bimsy"
__license__ = ""
__version__ = "0.1"
__status__ = "Production"


class RaspberryPiZipExplorer(AbstractZipExplorer):
    """
    Reads a zip file that may contain raspberry pi data for a single or multiple participants and
    puts the produced data frames in a dictionary, the keys will be the first character in the file name.
    """
    pd.options.mode.chained_assignment = None

    def __init__(self, raspberry_pi_zip_file: str) -> None:
        """
        Initializes Raspberry Pi parser and starts the processing of data.
        :param raspberry_pi_zip_file: A path to a zip that contains recorded Raspberry Pi data.
        """
        self._raspberry_pi_owners_dict_of_data_frames = {}
        self.void_zip_file_parser(raspberry_pi_zip_file)

    def void_zip_file_parser(self, raspberry_pi_zip_file: str) -> None:
        """
        Goes through the zip file and parses each within the zip and skips folders itself and empty files that are zero bytes.
        :param raspberry_pi_zip_file: String which represents the path to a zip file containing Raspberry Pi data.
        """
        with zipfile.ZipFile(raspberry_pi_zip_file) as opened_raspberry_zip:
            for file_path_name in opened_raspberry_zip.namelist():
                # A folder is not a file.
                if file_path_name.endswith("/") or file_path_name.endswith("\\"):
                    continue
                # Empty files are not compressed and do not contain info.
                elif int(str(opened_raspberry_zip.getinfo(file_path_name)).split(" ")[-1].split("=")[-1].strip(
                        ">")) == 0:
                    continue
                owner_identifier = file_path_name.split("/")[-1][0]
                with opened_raspberry_zip.open(file_path_name) as file_in_zip:
                    if owner_identifier in self._raspberry_pi_owners_dict_of_data_frames:
                        self._raspberry_pi_owners_dict_of_data_frames[owner_identifier].append(
                            RaspberryPiParser(file_in_zip).get_data_frame()
                        )
                    else:
                        # New owner will be made and the data for the first run will be added.
                        self._raspberry_pi_owners_dict_of_data_frames[owner_identifier] = [RaspberryPiParser(
                            file_in_zip).get_data_frame()]

    def get_dict_of_data_frames(self) -> dict:
        """
        Gives back a dictionary with data frames as values and owners as keys.
        :return: Dict of data frames with owners as key.
        """
        return self._raspberry_pi_owners_dict_of_data_frames
