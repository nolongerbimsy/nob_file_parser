from zipfile import ZipExtFile
import pandas as pd
from pandas.io.json import json_normalize
from parsers.json_parsers import general_json_parser


class HeartRateJSONParser(general_json_parser.GeneralJSONDataParser):
    """
    Parses JSON heart rate data.
    """

    def __init__(self, filename: ZipExtFile):
        """
        Process the whole heart rate JSON file.
        :param filename: String to the path of the file in the fitbit zip.
        """
        self._data_frame = self.parse_file(filename)
        self.void_organize_data_types()

    def void_organize_data_types(self) -> None:
        """
        Unfolds the dict/JSON within the data frame and adds it the row.
        """
        # Split value column into two as bpm and confidence columns.
        self._data_frame[["bpm", "confidence"]] = json_normalize((self._data_frame["value"]))
        # Drop the value column because it no longer used and the confidence and the bpm columns are extracted from it.
        self._data_frame.drop(["value"], axis=1, inplace=True)


    def get_data_frame(self) -> pd.DataFrame:
        """
        Gives back a data frame with heart rate data from a JSON file.
        :return: Data frame with heart rate data.
        """
        return self._data_frame
