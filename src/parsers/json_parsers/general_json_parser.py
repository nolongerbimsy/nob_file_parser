from zipfile import ZipExtFile
import pandas as pd
from parsers.abstract_data_parser import AbstractDataParser


class GeneralJSONDataParser(AbstractDataParser):

    def parse_file(self, filename: ZipExtFile) -> pd.DataFrame:
        """
        A generic parser for all JSON files.
        :param filename: Path to zip file that is going to be parsed.
        :return: A data frame with the data that originated from the submitted JSON
        """
        return pd.read_json(filename)

    def void_organize_data_types(self) -> None:
        """
        None, overwritten by subclass.
        """
        pass

    def get_data_frame(self) -> pd.DataFrame:
        """
        None, overwritten by subclass.
        """
        pass
