from zipfile import ZipExtFile
import pandas as pd
from pandas.io.json import json_normalize
from parsers.json_parsers.general_json_parser import GeneralJSONDataParser


class SleepJSONParser(GeneralJSONDataParser):
    """
    Parser Fitbit sleep JSON data.
    """

    def __init__(self, filename: ZipExtFile):
        """
        Using the general JSON parser it opens the json file. The sleep data is extracted and a
        dataframe with secondly sleep data is created.
        :param filename: The path and filename of the fitbit zip file.
        """
        self._data_frame = self.parse_file(filename)
        self.void_organize_data_types()

    def void_organize_data_types(self) -> None:
        """
        Process the data and put it into a dataframe
        """
        df = self._data_frame
        monthdf = pd.DataFrame()
        # loop over the days in a month
        for s in range(len(df)):
            # function to get the specified data type out of the json structure
            def getdata(df, type, di):
                data = df.levels[type]
                dfdata = pd.DataFrame(data)
                dfdata.index = (dfdata['dateTime'].str.replace("T", " "))
                dfdata['level'] = dfdata['level'].map(di)
                return dfdata

            # dictionary to convert strings to integers
            di = {"wake": 3, "light": 2, "rem": 1, "deep": 0, "awake": -3, "restless": -2, "asleep": -1}

            # get the sleep data called 'data' out of the json file
            dfdata = getdata(df.iloc[s], 'data', di)

            newdf = pd.DataFrame()
            # convert every row (begintime, sleep stage, duration) to a dataframe with the sleep stage per second
            # and add it to newdf
            for i in range(len(dfdata)):
                time = pd.date_range(dfdata.index[i], periods=dfdata.iloc[i].seconds, freq='s')
                newdf = newdf.append(pd.DataFrame({'level': dfdata.iloc[i].level}, index=time))

            # shortdata only exists if the participant slept longer than 3 hours
            try:
                # get the sleep data called 'shortData' out of the json file
                dfsdata = getdata(df.iloc[s], 'shortData', di)
                # convert every row (begintime, sleep stage, duration) to a dataframe with the sleep stage per second
                # the 'shortdata' overlaps with the 'data', so replace these values of the existing dataframe newdf
                # by the shortdata values
                for i in range(len(dfsdata)):
                    time = pd.date_range(dfsdata.index[i], periods=dfsdata.iloc[i].seconds, freq='s')
                    newdf.loc[time] = dfsdata.iloc[i].level
            except:
                pass
            # append it the the monthly dataframe
            monthdf = monthdf.append(newdf)
        self._data_frame = monthdf

    def get_data_frame(self) -> pd.DataFrame:
        """
        :return: A dataframe with the sleep stages per second.
        """
        return self._data_frame
