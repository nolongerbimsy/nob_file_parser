from zipfile import ZipExtFile
import pandas as pd
from pandas.io.json import json_normalize
from parsers.json_parsers.general_json_parser import GeneralJSONDataParser


class SleepSummaryJSONParser(GeneralJSONDataParser):
    """
    Parser Fitbit sleep summary JSON data.
    """

    def __init__(self, filename: ZipExtFile):
        """
        Using the general JSON parser it opens the json file. The sleep summary data is extracted and a
        dataframe with the sleep summary is created.
        :param filename: The path and filename of the fitbit zip file.
        """
        self._data_frame = self.parse_file(filename)
        self.void_organize_data_types()

    def void_organize_data_types(self) -> None:
        """
        Process the data and put it into a dataframe
        """
        sleep_data_frame = self._data_frame
        # Get the data from levels which contains Summary, shortData and Data. Select ONLY the Summary.
        summary_data_frame = json_normalize(sleep_data_frame["levels"]).iloc[:, 2:]
        # Select column names that contain minutes.
        summary_data_frame = summary_data_frame.filter(regex="minutes")
        # Select the phase from the column names (summary.PHASE.minutes).
        summary_data_frame.columns = summary_data_frame.columns.str.split(".").str[1]
        # Set index on sleeping date (not the time).
        # summary_data_frame = summary_data_frame.groupby("dateOfSleep").mean()
        # Get the index of the original data frame because that describes date of sleep.
        summary_data_frame.index = pd.to_datetime(sleep_data_frame["dateOfSleep"], format='%Y-%m-%d')
        # Remove duplicates that cause for double days.
        summary_data_frame.index.name = None
        summary_data_frame = summary_data_frame.groupby(level=0).mean()
        self._data_frame = summary_data_frame

    def get_data_frame(self) -> pd.DataFrame:
        """
        :return: A dataframe with the sleep summary.
        """
        return self._data_frame
