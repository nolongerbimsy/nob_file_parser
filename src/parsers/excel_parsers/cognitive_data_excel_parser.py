import pandas as pd


class CognitiveDataExcelParser:
    """
    Parses the excel file with cognitive data.
    """

    def __init__(self, filename: str):
        """
        Parses the the excel file and returns a parsed pandas data frame of the excel data.
        :param filename: String of path to filename.
        """
        self._dictionary_of_excel_sheet_data_frames = self.parse_excel_file(filename)
        self.void_organize_data_types()

    @staticmethod
    def parse_excel_file(filename: str) -> dict:
        """
        Reads the excel file and skips bad lines and reads all sheets that reside within the excel file.
        :param filename: String of path to filename.
        :return: Data frame of the cognitive excel data.
        """
        return pd.read_excel(filename,
                             sheet_name=None,
                             error_bad_lines=False,
                             converters={"Date": str, "Time": str})

    def void_organize_data_types(self) -> None:
        """
        Organizes per sheet the date time and the cognitive scores which are appended to the data frame.
        """
        for sheet in self._dictionary_of_excel_sheet_data_frames:
            excel_data_sheet = self._dictionary_of_excel_sheet_data_frames[sheet]
            self.void_reorganize_date_time_column(excel_data_sheet)
            excel_data_sheet = excel_data_sheet.assign(
                **dict(zip(["Cognitive_Score", "Raw_math_score", "Raw_reaction_score"], [
                    *self.calculate_cognitive_score(
                        excel_data_sheet[["RT_ave", "Math_good", "Math_wrong", "RT_ave.1"]])])))
            self._dictionary_of_excel_sheet_data_frames[sheet] = excel_data_sheet

    @staticmethod
    def calculate_cognitive_score(cognitive_formula_data: pd.DataFrame) -> tuple:
        """
        Calculates the cognitive score based on the values given within the excel file.
        :param cognitive_formula_data: Input data for the cognitive score calculations.
        :return: A tuple of a weighted cognitive score and the raw math and reaction scores.
        """
        # Cognitive score formula.
        # (
        # Math with weight factor.
        # 0.5 * (math_score=(right-wrong)/40) +
        # Reaction with weight factor.
        # 0.5 * (200/(average_reaction time of both reaction time tests))
        # )
        # * 100
        cognitive_score = (
                0.5 * ((cognitive_formula_data["Math_good"] - cognitive_formula_data["Math_wrong"]) / 40) + 0.5 * (
                200 / ((cognitive_formula_data["RT_ave"] + cognitive_formula_data["RT_ave.1"]) / 2)) * 100)
        raw_math_score = ((cognitive_formula_data["Math_good"] - cognitive_formula_data["Math_wrong"]) / 40)
        raw_reaction_score = 200 / ((cognitive_formula_data["RT_ave"] + cognitive_formula_data["RT_ave.1"]) / 2)
        return cognitive_score, raw_math_score, raw_reaction_score

    @staticmethod
    def void_reorganize_date_time_column(single_data_frame: pd.DataFrame) -> None:
        """
        Alter the date time from a data frame and make it the index.
        :param single_data_frame: Data frame based on a sheet of a single owner.
        """
        # Make datetime index.
        single_data_frame["Datetime"] = pd.to_datetime(
            (single_data_frame["Date"] + " " + single_data_frame["Time"])
                .str.split(" ").str[0], format='%Y-%m-%d')
        # Set datetime column as index.
        single_data_frame.set_index('Datetime', inplace=True)
        # Drop date column.
        single_data_frame.drop(["Date"], axis=1, inplace=True)

    def get_dict_of_cognitive_data_frames(self) -> dict:
        """
        Gives back a dictionary with all sheets and cognitive data.
        :return: Dict with owners as keys and data frames as values.
        """
        return self._dictionary_of_excel_sheet_data_frames
