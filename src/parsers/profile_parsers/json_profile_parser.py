import json
from zipfile import ZipExtFile
from parsers.profile_parsers.general_profile_parser import GeneralProfileParser


class JSONProfileParser(GeneralProfileParser):
    """
    JSON parser that can parse FitBit JSON profiles.
    """

    def __init__(self, filename: ZipExtFile):
        """
        Creates an object that collects the filename from a JSON profile made by Fitbit.
        :param filename: Filename of profile.
        """
        read_profile = self.read_profile_file(filename)
        self._profile_name = self.filter_name(read_profile)

    def filter_name(self, opened_profile: list) -> str:
        return json.loads(opened_profile[0])["fullName"].replace(" ", "").lower()

    def get_profile_name(self) -> str:
        return str(self._profile_name)
