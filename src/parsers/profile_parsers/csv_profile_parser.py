from zipfile import ZipExtFile
from parsers.profile_parsers.general_profile_parser import GeneralProfileParser


class CSVProfileParser(GeneralProfileParser):
    """
    CSV parser that can parse FitBit CSV profiles.
    """

    def __init__(self, filename: ZipExtFile) -> None:
        """
        Creates an object that collects the filename from a CSV profile made by Fitbit.
        :param filename: Filename of profile.
        """
        read_profile = self.read_profile_file(filename)
        self._profile_name = self.filter_name(read_profile)

    def filter_name(self, profile_lines: list) -> str:
        """
        Collect user name from list element.
        :param profile_lines: List of lines that make-up the user profile.
        :return: A string with the user profile name without white spaces.
        """
        return profile_lines[1].split(b",")[1].replace(b" ", b"").lower().decode("utf-8")

    def get_profile_name(self) -> str:
        return str(self._profile_name)
