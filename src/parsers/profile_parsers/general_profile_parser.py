from zipfile import ZipExtFile


class GeneralProfileParser:
    """
    General layout of profile parsers.
    """

    @staticmethod
    def read_profile_file(filename: ZipExtFile) -> list:
        """
        Opens csv file and reads the lines of the CSV.
        :param filename: Name of the CSV file that is read.
        :return: A list with all lines in the profile.
        """
        return filename.readlines()

    def filter_name(self, profile_lines: list) -> str:
        """
        Collect user name from list element.
        :param profile_lines: List of lines that make-up the user profile.
        :return: A string with the user profile name without white spaces.
        """
        pass
