import pandas as pd
from parsers.csv_parsers.general_csv_parser import GeneralCSVDataParser


class CognitiveDataCSVParser(GeneralCSVDataParser):
    """
    Parses the csv of cognitive data, not used.
    """
    def __init__(self, filename: str):
        """
        Parses a csv with cognitive data.
        :param filename: Path to zipfile.
        """
        self._data_frame = self.parse_file(filename)
        self.void_organize_data_types()

    def void_organize_data_types(self) -> None:
        """
        Modifies the datetime column of the data frame and makes it further usable for processing.
        """
        self._data_frame["DateTime"] = self._data_frame["Date"].map(str) + " " + self._data_frame["Time"]
        self._data_frame["DateTime"] = pd.to_datetime(self._data_frame["DateTime"])
        self._data_frame["DateTime"] = self._data_frame.set_index('DateTime', inplace=True)
        self._data_frame = self._data_frame.drop(["Date", "Time", "DateTime"], axis=1)

    def get_data_frame(self) -> pd.DataFrame:
        """
        Gives back a data frame for a single person.
        :return: Data frame with cognitive data.
        """
        return self._data_frame
