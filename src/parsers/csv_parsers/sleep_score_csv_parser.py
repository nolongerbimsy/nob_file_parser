import pandas as pd
from parsers.csv_parsers.general_csv_parser import GeneralCSVDataParser


class SleepScoreCSVParser(GeneralCSVDataParser):
    """
    Parser Fitbit sleep score SCV data.
    """
    def __init__(self, filename: str):
        """
        Using the general CSV parser it opens the sleep score CSV file. The sleep score is extracted and a
        dataframe with the sleep score is created.
        :param filename: The path and filename of the fitbit zip file.
        """
        self._data_frame = self.parse_file(filename)
        self.void_organize_data_types()

    def void_organize_data_types(self) -> None:
        """
        Process the sleep score data and puts it into a data frame.
        """

        sleep_score_data_frame = self._data_frame
        sleep_score_data_frame.index = pd.to_datetime(
            sleep_score_data_frame[sleep_score_data_frame.columns[1]].str.split("T").str[0], format='%Y-%m-%d'
        )
        sleep_score_data_frame.drop(sleep_score_data_frame[sleep_score_data_frame.columns[[0, 1]]], axis=1,
                                    inplace=True)
        sleep_score_data_frame = sleep_score_data_frame.groupby("timestamp").mean()
        sleep_score_data_frame.index.name = None
        self._data_frame = sleep_score_data_frame

    def get_data_frame(self) -> pd.DataFrame:
        """
        :return: A dataframe with the sleep scores.
        """
        return self._data_frame
