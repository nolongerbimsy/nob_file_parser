import pandas as pd
from parsers.csv_parsers.general_csv_parser import GeneralCSVDataParser


class RaspberryPiParser(GeneralCSVDataParser):
    """
    Parses Raspberry pi CSV data.
    """

    def __init__(self, filename: str) -> None:
        """
        Process the data and makes a data frame out of it.
        :param filename: String of filename and path.
        """
        self._data_frame = self.parse_file(filename, header=None)
        self.void_organize_data_types()

    def void_organize_data_types(self) -> None:
        """
        Gets the relevant columns from the Raspberry Pi csv files also applies the
        appropriate column names for Raspberry pi.
        """
        # Select the appropriate columns,
        # this is not possible in the read csv because it messes with how the data is read and causes errors.
        self._data_frame = self._data_frame.iloc[:, :5]
        raspberry_date_time_column_name = self._data_frame.columns[0]

        # Remove units that are less than a second and removes NaT/NaN data.
        self._data_frame[raspberry_date_time_column_name] = self._data_frame[raspberry_date_time_column_name].str.split(
            ".", expand=True)[0]
        self._data_frame = self._data_frame[self._data_frame[raspberry_date_time_column_name].notnull()]
        # Set the datetime format, just to make a standard for all the Raspberry Pi data.
        self._data_frame[raspberry_date_time_column_name] = pd.to_datetime(
            self._data_frame[raspberry_date_time_column_name], format='%Y-%m-%d %H:%M:%S')
        self._data_frame.rename(
            columns=dict(zip(self._data_frame.columns[1:], ["Air pressure", "Humidity", "Temperature", "Lux"])),
            inplace=True)

    def get_data_frame(self) -> pd.DataFrame:
        """
        Gives back a data frame with Raspberry Pi data.
        :return: A data frame with Raspberry pi data.
        """
        return self._data_frame
