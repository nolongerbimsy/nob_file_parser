import pandas as pd
from parsers.abstract_data_parser import AbstractDataParser


class GeneralCSVDataParser(AbstractDataParser):

    def parse_file(self, filename: str, **parser_keyword_arguments: object) -> pd.DataFrame:
        """
        A generic parser for all CSV files, extra options can be activated with unpacking parser_keyword_arguments.
        :param filename: Path to zip file that is going to be parsed.
        :param parser_keyword_arguments: Allows multiple arguments specific for each parser.
        :return: A data frame with the data that originated from the submitted CSV.
        """
        return pd.read_csv(filename,
                           error_bad_lines=False,
                           **parser_keyword_arguments)

    def void_organize_data_types(self) -> None:
        """
        None, overwritten by subclass.
        """
        pass

    def get_data_frame(self) -> pd.DataFrame:
        """
        None, overwritten by subclass.
        """
        pass
