import sys
import time
import json
import os
import pathlib
from functools import reduce
from typing import List

import pandas as pd

from explorers.fitbit_zip_explorer import FitBitZipFileExplorer
from explorers.raspberry_pi_zip_explorer import RaspberryPiZipExplorer
from parsers.excel_parsers.cognitive_data_excel_parser import CognitiveDataExcelParser
from processors import raspberry_pi_csv_processor, heart_rate_json_processor, sleep_json_processor, \
    sleep_summary_json_processor


def load_in_pseudo_names(json_file: str) -> dict:
    """
    Allows to use a json file which masks the original owner names.
    {"A": 1, "B":  2, "C":  3, "Z": 4}
    :param json_file:
    :return:
    """
    if str(json_file).split(".")[-1] == "json":
        with open(json_file, "r") as opened_pseudo_name_config:
            return json.load(opened_pseudo_name_config)


def search_file_extensions(specified_directory: str, glob_file_regular_expressions: List[str]) -> dict:
    """
    With the lazy sub command it searches through a folder to find the appropriate files that should be processed.
    :param specified_directory: The path to the directory where all files are stored for lazy search.
    :param glob_file_regular_expressions: Internally specified to look for certain file extensions,
    currently it looks for zip, xlsx and a config.json file.
    :return: A dictionary with all files found that are according to the extensions and names.
    """
    dict_of_found_file_types = {}
    for glob_file_regular_expression in glob_file_regular_expressions:
        found_files = list(pathlib.Path(specified_directory).rglob(glob_file_regular_expression))
        file_extension_key = glob_file_regular_expression.split(".")[-1]
        if len(found_files) != 1:
            dict_of_found_file_types[file_extension_key] = found_files
        else:
            dict_of_found_file_types[file_extension_key] = found_files[0]
    return dict_of_found_file_types


def zip_file_identifier(input_bytes: str) -> bool:
    """
    A simple test to determine if a file is a zip file based on the first characters in hexadecimal.
    :param input_bytes:
    :return: It tells if it IS a zip or NOT based on these characters.
    """
    with open(input_bytes, "rb") as file_bytes:
        # Zip bytes.
        if file_bytes.read(4).hex().lower() == "504b0304":
            return True


def zip_file_separator(zip_files: list) -> tuple:
    """
    Separates the raspberry pi data zips from the fitbit data zips and puts them in the appropriate lists.
    :param zip_files: List of all collected files that have a zip header and extension found by the lazy method.
    :return: A tuple which contains two lists, one with raspberry pi zips, the other with fitbit zips.
    """
    raspberry_zip = []
    fitbit_zip = []
    for zip_file in zip_files:
        if zip_file_identifier(zip_file) and ("fitbit" in str(zip_file).lower()):
            fitbit_zip.append(zip_file)
        elif zip_file_identifier(zip_file) and ("raspberry_pi" in str(zip_file).lower()):
            raspberry_zip.append(zip_file)
    return raspberry_zip, fitbit_zip


def pseudonymize_dict(original_dict: dict, pseudo_key_dict: dict) -> dict:
    """
    Transfers dictionary values from a key with an original name to a pseudonymized key.
    :param original_dict: A processed dictionary with the original owner keys.
    :param pseudo_key_dict: A dictionary that describes which name should have which key.
    :return: Data dictionary with pseudonymized keys
    """
    new_dict = {}
    for original_key in original_dict.keys():
        try:
            new_dict[pseudo_key_dict[original_key[0].upper()]] = original_dict[original_key]
        except KeyError:
            new_dict[original_key] = original_dict[original_key]
    return new_dict


def pseudonymize_key(original_key: str, pseudo_key_dict: dict) -> int:
    """
    Converts the key for a single original key to a pseudonymized key.
    :param original_key: Key that that should be pseudonymized.
    :param pseudo_key_dict: Dictionary wherein the original keys are specified.
    :return: A pseudonymized key.
    """
    try:
        final_key = pseudo_key_dict[original_key[0].upper()]
    except KeyError:
        final_key = original_key
    return final_key


def merge_data_frames(*data_frames: pd.DataFrame) -> pd.DataFrame:
    """
    Merges an unspecified amount of smaller data frames into a single large one.
    :param data_frames: multiple data frames.
    :return: A single large data frame.
    """
    return reduce(lambda x, y: pd.merge(x, y, left_index=True, right_index=True, how="outer"),
                  list(data_frames))


def making_secondly_daily_data_profile_folders(directory: str, profile_name: str, separator_char: str) -> dict:
    """
    Makes a profile folder consisting of the profile name and creates three sub directories for daily,
    secondly and minutely data wherein each of the produced data frames can be stored in.
    :param directory: The directory where the subdirectories should be made.
    :param profile_name: The name of the profile owner which will be used for the name of the profile folder.
    :param separator_char: Depending on the operating system the name ending can be different, NOT TESTED ON WINDOWS.
    :return: A dictionary with paths specified to the sub directories for each data type to easily find it later on.
    """
    dict_of_produced_folder_paths = {}
    produced_folder_names = ["minutely_data", "secondly_data", "daily_data"]
    for data_type in produced_folder_names:
        folder_name = os.path.join(directory, f"Profile_{profile_name}", data_type) + separator_char
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)
        dict_of_produced_folder_paths[data_type.split("_")[0]] = folder_name
    return dict_of_produced_folder_paths


def resample_data_from_second_to_minute(secondly_data_frame: pd.DataFrame) -> pd.DataFrame:
    """
    Transform secondly data into minutely data.
    :param secondly_data_frame:
    :return:
    """
    return secondly_data_frame.resample("1T").mean()


def group_data_frame_by_sleeping_day(data_frame_with_time_unit: pd.DataFrame) -> dict:
    """
    Makes groups per sleeping period, sleeping periods start and end after 12 o'clock in the morning because
    participants can go late to bed and wake up late. Group names are based on the end of the sleeping period.
    :param data_frame_with_time_unit: A data frame which contains measurements for a time unit.
    :return: Dictionary of sleeping period dates which contains data frames of the corresponding time units.
    """
    dict_of_data_frames_with_measured_days = {}
    #  Identify on the sleep state whether the data is relevant.
    # Remove duplicates that appeared with querying for not null.
    queried_data = data_frame_with_time_unit[~data_frame_with_time_unit.index.duplicated(keep="first")]
    # Add time difference to collect the differences in time.
    measured_days = list(pd.date_range(queried_data.index[0], queried_data.index[-1]).date)
    for index_of_date in range(len(measured_days)):
        # There is no day after the last day so quit before it arrives the last one.
        if measured_days[index_of_date] == measured_days[-1]:
            continue
        # Add the the time frame from 12 to 12, but remove the data that has no sleep level.
        dict_of_data_frames_with_measured_days[measured_days[index_of_date + 1]] = queried_data[str(
            measured_days[index_of_date]) + " 12:00:00": str(
            measured_days[index_of_date + 1]) + " 12:00:00"].dropna(subset=["level"])

    return dict_of_data_frames_with_measured_days


def produce_daily_averages_of_raspberry_pi_data(grouped_sleeping_periods: dict) -> pd.DataFrame:
    """
    Queries per day the columns of: Air pressure, Humidity, Temperature and Lux and calculates the averages from the
    data frame.
    :param grouped_sleeping_periods: Dict which contains dates with data frames.
    :return: Single data frame with all daily averages.
    """
    dates = list(grouped_sleeping_periods.keys())
    daily_averages = []
    for end_of_sleeping_date in grouped_sleeping_periods:
        daily_averages.append(
            grouped_sleeping_periods[end_of_sleeping_date][["Air pressure", "Humidity", "Temperature", "Lux"]].mean())
    return pd.DataFrame(daily_averages, index=pd.Series(pd.to_datetime(dates, format="%Y-%m-%d"))).groupby(
        level=0).mean()


def write_data_to_csv_files(grouped_sleeping_periods: dict, profile_owner: str, data_directory: str,
                            identifier_string: str) -> None:
    """
    Writes the specified data to csv files.
    :param grouped_sleeping_periods: Data that should be written to csv files.
    :param profile_owner: String of the owner of the profile.
    :param data_directory: String to the path of the folder where the appropriate file should be stored.
    :param identifier_string: String which describes what type of unit is associated with the data type.
    """
    for end_of_sleeping_date, time_period_data_frame in grouped_sleeping_periods.items():
        file_date = end_of_sleeping_date
        with open(f"{data_directory}{profile_owner}_{identifier_string}_{file_date}_{end_of_sleeping_date}.csv",
                  "w") as out_file:
            time_period_data_frame.to_csv(out_file)


def main():
    """
    Executes the whole file parser.
    """
    start = time.time()
    if sys.platform == "win32":
        seperator_character = "\\"
    else:
        seperator_character = "/"
    # Resolve the path to the data folder.
    folder = os.path.abspath(sys.argv[1])

    all_raspberry_pi_data = {}

    found_files = search_file_extensions(folder, ["*.zip", "*.xlsx", "config.json"])
    pseudo_names_dict = load_in_pseudo_names(found_files["json"])
    raspberry_pi_zips, fitbit_zips = zip_file_separator(found_files["zip"])
    # Cognitive data.
    cognitive_data = pseudonymize_dict(
        CognitiveDataExcelParser(found_files["xlsx"]).get_dict_of_cognitive_data_frames(),
        pseudo_names_dict)

    # Raspberry Pi, in general a single zip.
    for raspberry_pi_zip in raspberry_pi_zips:
        raspberry_pi_data = RaspberryPiZipExplorer(str(raspberry_pi_zip))
        raspberry_pi_data = raspberry_pi_csv_processor.RaspberryPiCSVListProcessor(
            raspberry_pi_data.get_dict_of_data_frames(), start_time="2019-11-02",
            end_time="2019-12-21").get_data_frames()
        # Modifying the keys:
        raspberry_pi_data = pseudonymize_dict(raspberry_pi_data, pseudo_names_dict)
        all_raspberry_pi_data = {**all_raspberry_pi_data, **raspberry_pi_data}
    print("Done processing raspberry pi data.")

    # Fitbit
    for fitbit_zip in fitbit_zips:
        parsed_fitibit_data = FitBitZipFileExplorer(fitbit_zip)
        pseudo_profile_owner = pseudonymize_key(parsed_fitibit_data.get_profile_owner(), pseudo_names_dict)
        print(f"Finished reading fitbit data for profile {pseudo_profile_owner}.")
        # heart rate
        heart = heart_rate_json_processor.HeartRateJSONDataListProcessor(
            parsed_fitibit_data.get_json_heart_rate_data_frames(), start_time="2019-11-02",
            end_time="2019-12-21").get_data_frame()
        print(f"Done processing heart rate data for profile {pseudo_profile_owner}.")

        # sleep data
        sleep = sleep_json_processor.SleepJSONDataListProcessor(
            parsed_fitibit_data.get_json_sleep_data_frames(), start_time="2019-11-02",
            end_time="2019-12-21").get_data_frame()
        print(f"Done processing sleep json data for profile {pseudo_profile_owner}.")

        # sleep summary data
        sleep_summary = sleep_summary_json_processor.SleepSummaryJSONProcessor(
            parsed_fitibit_data.get_json_sleep_summary_data_frames()).get_data_frame()
        print(f"Done processing sleep summary data for profile {pseudo_profile_owner}.")

        # sleep score data
        sleep_score = parsed_fitibit_data.get_csv_sleep_score_data_frame()

        secondly_data = merge_data_frames(all_raspberry_pi_data[pseudo_profile_owner], heart, sleep)
        print(f"Finished merging secondly data for profile {pseudo_profile_owner}.")

        # Remove the first minute error which is left empty by the raspberry pi data.
        secondly_data[secondly_data.columns[0:4]] = secondly_data[secondly_data.columns[0:4]].bfill()

        # Making folder for a specific profile.
        print(f"Creating folders for profile {pseudo_profile_owner}.")
        folder_file_paths = making_secondly_daily_data_profile_folders(folder, str(pseudo_profile_owner),
                                                                       seperator_character)
        minutely_data = resample_data_from_second_to_minute(secondly_data)

        group_by_night_secondly = group_data_frame_by_sleeping_day(secondly_data)
        write_data_to_csv_files(group_by_night_secondly, str(pseudo_profile_owner), folder_file_paths["secondly"],
                                "second")
        print(f"Finished writing secondly files for profile {pseudo_profile_owner}.")
        group_by_night_minutely = group_data_frame_by_sleeping_day(minutely_data)

        write_data_to_csv_files(group_by_night_minutely, str(pseudo_profile_owner), folder_file_paths["minutely"],
                                "minute")
        print(f"Finished writing minutely files for profile {pseudo_profile_owner}.")
        raspberry_pi_daily_averages = produce_daily_averages_of_raspberry_pi_data(group_by_night_secondly)

        daily_data = merge_data_frames(cognitive_data[pseudo_profile_owner], sleep_score, sleep_summary,
                                       raspberry_pi_daily_averages)
        # Removes duplicates based on the whole entry, indices that appear multiple times but are different are kept.
        daily_data = daily_data[~daily_data.duplicated(subset=None, keep="first")]

        with open("{}daily_data_{}.csv".format(folder_file_paths["daily"], pseudo_profile_owner),
                  "w") as daily_data_frame:
            daily_data.to_csv(daily_data_frame)
        print(f"Finished writing daily file for profile {pseudo_profile_owner}.")

    print(time.time() - start)


if __name__ == "__main__":
    sys.exit(main())
