# Nob file parser

## Background

This file parser has been written to gain insight in sleeping and waking up behavior of participants within a quantified
self project of the master: [Data Science for Life Sciences](https://www.hanze.nl/nld/onderwijs/techniek/instituut-voor-life-science--technology/opleidingen/master/data-science-for-life-sciences)
from the Hanze university of Applied Sciences. With this parser FitBit and [Raspberry Pi](https://bitbucket.org/nolongerbimsy/nob_raspberry_pi/src) 
data can be processed into data frames to gain [insight](https://bitbucket.org/nolongerbimsy/nob_webapp/src) into sleeping behavior, environment and cognitive function.  

WITHIN THIS REPOSITORY NO REAL PARTICIPANT DATA IS AVAILABLE.

## Requirements:

#### Hardware
- [FitBit Versa 2](https://www.fitbit.com/us/products/smartwatches/versa)


#### Software
- [Python 3](https://www.python.org/)
- [Pandas (0.25.3)](https://pandas.pydata.org/)
- [xlrd (1.2.0)](http://www.python-excel.org/)

Install all required packages at once:

    pip3 install xlrd pandas

## How to run
The program needs to run from the commandline and requires only a single positional argument.    
The argument should be the folder wherein all data is stored. (FitBit profile data downloaded from the FitBit website, Raspberry Pi CSV and and excel sheet, the JSON is optional.)
The output will end up in the same folder as the folder that was specified as input.
WARNING: It will only work if there is ONLY 1 excel file and/or ONLY 1 JSON file, multiple files is going to causes errors!

config.json example:

    {"A": 1, "B":  2, "C":  3, "Z": 4}

Example input folder structure:

    /Users/mini_mi/my_data
    |____mini_me_cognitive_data.xlsx
    |____Raspberry_Pi_data
    | |____raspberry_pi.zip
    |____config.json
    | |____Mini_ME_fitbit
    | | |____MyFitbitData.zip 

Example input command:

    My_system:mini_mi$ python3 backend_main.py /Users/mini_mi/my_data/
   
Example output:

    /Users/mini_mi/my_data
    |____mini_me_cognitive_data.xlsx
    |____Raspberry_Pi_data
    | |____raspberry_pi.zip
    |____config.json
    | |____Mini_ME_fitbit
    | | |____MyFitbitData.zip
    | 
    |____Profile_X
    | |____daily_data
    | | |____daily_data_X.csv
    | |____minutely_data
    | | |____X_minute_2019-11-04_2019-11-04.csv
    | | |____X_minute_2019-11-05_2019-11-05.csv
    | | |____X_minute_2019-11-06_2019-11-06.csv
    | | |____X_minute_2019-11-07_2019-11-07.csv
    | |____secondly_data
    | | |____X_second_2019-11-04_2019-11-04.csv
    | | |____X_second_2019-11-05_2019-11-05.csv
    | | |____X_second_2019-11-06_2019-11-06.csv
    | | |____X_second_2019-11-07_2019-11-07.csv
